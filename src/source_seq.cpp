#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include <limits.h>
using namespace std;

#define MAX_VALUE 100
#define MIN_VALUE 0

#define FORWARD_MODE 1
#define BACKWARD_MODE 2


int read_parameters(int *child_count, int *ringe_mode, int *max_length, int *min_length);

int parent_work();

int next_prev(int *childs_ids, int childs_count, int ring_mode, int min_length, int max_length);

int* generate_array(int min_length, int max_length, int *size);

int* adjust_array(int *old_array, int old_size, int *new_size, int min_length, int max_length);

int randInt(int min, int max);

void fill_array(int *array, int offset, int size);

string array_to_string(int *array, int size);

int main(int argc, char* argv[]) {
	srand(time(0));
	parent_work();
	int temp; cin >> temp;
	return 0;
}

int read_parameters(int *child_count, int *ringe_mode, int *max_length, int *min_length) {

	cout << "Enter number of childs\n";
	cin >> *child_count;
	if (*child_count <= 0){
		cout << "positive Number for childs count\n";
		return -1;
	}


	cout << "Enter Ring Mode 1 for Forward and 2 for backword\n";
	cin >> *ringe_mode;
	if (*ringe_mode != FORWARD_MODE && *ringe_mode != BACKWARD_MODE){
		cout << "only 1 or 2 for ringe mode\n";
		return -1;
	}


	cout << "Enter Min Length\n";
	cin >> *min_length;
	if (*min_length <= 0) {
		cout << "positive Number for Min Length\n";
		return -1;
	}

	cout << "Enter Max Length\n";
	cin >> *max_length;
	if (*max_length <= 0) {
		cout << "positive Number for Max Length\n";
		return -1;
	}

	if (*max_length < *min_length){
		cout << "Max Length less than Min Length\n";
		return -1;
	}

	return 1;
}

int parent_work() {
	int childs_count = 3, ring_mode = 1, max_length = 10, min_length = 1;
	read_parameters(&childs_count, &ring_mode, &max_length, &min_length);
	clock_t tStart = clock();

	int array_size;
	int *parent_array = generate_array(min_length, max_length, &array_size);
	cout<<"\nparent:  size: "<< array_size <<", elements: "<<array_to_string(parent_array, array_size);
	if (ring_mode == 1){
		for (int i = 0; i < childs_count; i++){
			int new_array_size;
			int *new_array = adjust_array(parent_array, array_size, &new_array_size, min_length, max_length);
			cout << "\nchild[" << i + 1 << "]:size: " << new_array_size << ", elements: " << array_to_string(new_array, new_array_size);

			array_size = new_array_size;
			parent_array = new int[new_array_size];
			for (int i = 0; i < new_array_size; i++){
				parent_array[i] = new_array[i];
			}
		}
	}
	else{
		for (int i = childs_count; i > 0; i--){
			int new_array_size;
			int *new_array = adjust_array(parent_array, array_size, &new_array_size, min_length, max_length);
			cout << "\nchild[" << i  << "]:size: " << new_array_size << ", elements: " << array_to_string(new_array, new_array_size);

			array_size = new_array_size;
			parent_array = new int[new_array_size];
			for (int i = 0; i < new_array_size; i++){
				parent_array[i] = new_array[i];
			}
		}
	}
	printf("\nTime taken: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
	return 0;
}


int* adjust_array(int *old_array, int old_size, int *new_size, int min_length, int max_length) {
	*new_size = randInt(min_length, max_length);

	int* new_array = new int[*new_size];
	if (*new_size > old_size) {
		for (int i = 0; i < old_size; i++)
			new_array[i] = old_array[i];
		fill_array(new_array, old_size, *new_size);
	}
	else {
		for (int i = 0; i < *new_size; i++)
			new_array[i] = old_array[i];
	}
	return new_array;
}

int* generate_array(int minLengh, int maxLengh, int *size) {
	*size = randInt(minLengh, maxLengh);
	int *array = new int[*size];
	fill_array(array, 0, *size);
	return array;
}

string array_to_string(int *array, int size){
	stringstream str;
	str << "[";
	for (int i = 0; i < size; i++) {
		str << array[i];
		if (i < size - 1){
			str << ",";
		}
	}
	str << "]";
	return str.str();
}

void fill_array(int *array, int offset, int size) {
	for (int i = offset; i < size; i++)
		array[i] = randInt(MIN_VALUE, MAX_VALUE);
}

int randInt(int min, int max) {
	return (rand() % (max - min)) + min;
}
