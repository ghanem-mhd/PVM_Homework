#include <stdio.h>
#include <iostream>
#include <random>
#include <time.h>
#include <sstream>
#include <limits.h>
#include "pvm3.h"
using namespace std;

#define MAX_VALUE 100
#define MIN_VALUE 0

#define FORWARD_MODE 1
#define BACKWARD_MODE 2

#define NEXT_PREV_TAG 	1000
#define ARRAY_TAG		1005

int read_parameters(int *child_count, int *ringe_mode, int *max_length, int *min_length);

int parent_word();

int next_prev(int *childs_ids,int childs_count,int ring_mode,int min_length,int max_length);

int child_work();

int* generate_array(int min_length, int max_length, int *size);

int* adjust_array(int *old_array, int old_size, int *new_size, int min_length,int max_length);

int randInt(int min, int max);

void fill_array(int *array, int offset, int size);

string array_to_string(int *array, int size);

int main(int argc, char* argv[]) {
	int tid = pvm_mytid();
	int pid = pvm_parent();

	if (tid < 0) {
		pvm_perror(argv[0]);
		pvm_exit();

		return -1;
	}

	if (pid == PvmNoParent || pid == PvmParentNotSet) {
		clock_t tStart = clock();
		parent_word();
		printf("Time taken: %.2f miliseconds\n", (double)(clock() - tStart));
		pvm_exit();
	} else {
		child_work();
		pvm_exit();
	}
	return 0;
}

int read_parameters(int *child_count, int *ringe_mode, int *max_length, int *min_length) {

	cout << "Enter number of childs\n";
	cin >> *child_count;
	if (*child_count <= 0){
		cout << "positive Number for childs count\n";
		return -1;
	}

	cout << "Enter Ring Mode 1 for Forward and 2 for backword\n";
	cin >> *ringe_mode;
	if (*ringe_mode != FORWARD_MODE && *ringe_mode != BACKWARD_MODE){
		cout << "only 1 or 2 for ringe mode\n";
		return -1;
	}

	cout << "Enter Min Length\n";
	cin >> *min_length;
	if (*min_length <= 0) {
		cout << "positive Number for Min Length\n";
		return -1;
	}

	cout << "Enter Max Length\n";
	cin >> *max_length;
	if (*max_length <= 0) {
		cout << "positive Number for Max Length\n";
		return -1;
	}

	if (*max_length < *min_length){
		cout << "Max Length less than Min Length\n";
		return -1;
	}

	return 1;
}

int parent_word() {
	// Default values
	int childs_count = 3,
		ring_mode = 1,
		max_length = 10,
		min_length = 1,
		*childs_ids = 0,
		info;

	info = read_parameters(&childs_count,&ring_mode,&max_length,&min_length);

	if (info == -1){
		return -1;
	}

	childs_ids = new int[childs_count];

	//TODO change the path
	info = pvm_spawn("/home/mo7ammed/workspace/Random_Ringe_Project/Debug/Random_Ringe_Project", (char**)0,
			PvmTaskDefault,(char*)0, childs_count, childs_ids);

	if (info != childs_count) {
		return -1;
	}

	cout << endl;

	cout << "Parent ID\t\t" 
		 << pvm_mytid() 
		 << endl;

	cout << "Childreen Ids\t\t" 
		 << array_to_string(childs_ids,childs_count)
		 << endl;

	//Generate array with random length with random element
	int array_size;
	int *parent_array = generate_array(min_length, max_length,&array_size);

	cout << "Parent Array\t\t"
		 << array_to_string(parent_array, array_size) 
		 << endl;

	//Send next Prev to childreen
	next_prev(childs_ids,childs_count,ring_mode,min_length,max_length);


	//Send Generated Array to first child or last child
	pvm_initsend(PvmDataDefault);
	pvm_pkint(&array_size, 1, 1);
	pvm_pkint(parent_array, array_size, 1);
	if (ring_mode == FORWARD_MODE)
		pvm_send(childs_ids[0], ARRAY_TAG);
	else
		pvm_send(childs_ids[childs_count-1], ARRAY_TAG);


	cout << endl
		 << "---------------Recived From Childreen----------------"
		 << endl;

	//Receive from childreen
	for(int i = 0 ; i < childs_count ; i++){

		int my_id,my_next,my_prev,recived_array_size,sended_array_size;
		pvm_recv(-1, ARRAY_TAG);

		pvm_upkint(&my_id,1,1);
		pvm_upkint(&my_next,1,1);
		pvm_upkint(&my_prev,1,1);

		pvm_upkint(&recived_array_size,1,1);
		int * recived_array = new int [recived_array_size];
		pvm_upkint(recived_array,recived_array_size, 1);

		pvm_upkint(&sended_array_size,1,1);
		int * sended_array = new int [sended_array_size];
		pvm_upkint(sended_array,sended_array_size, 1);

		cout << "I am " << my_id
			 << " my Next is " << my_next 
			 << " and my Prev is " << my_prev  
			 << endl;

		cout << "Recived Array " 
			 << array_to_string(recived_array,recived_array_size) 
			 << endl;

		cout << "Sended Array  "
			 << array_to_string(sended_array,sended_array_size) 
			 << endl;

		cout << "-----------------------------------------------------"
			 << endl;

		delete [] recived_array;
		delete [] sended_array;
	}
	return 0;
}

int child_work(){
	int tid = pvm_mytid();
	int recived_array_size,
		next, prev,
		min_length, 
		max_length,
		*recived_array = 0;


	//First Recive next and prev id
		pvm_recv(-1, NEXT_PREV_TAG);
		pvm_upkint(&next, 1, 1);
		pvm_upkint(&prev, 1, 1);
		pvm_upkint(&min_length, 1, 1);
		pvm_upkint(&max_length, 1, 1);
		
		cout << "I am " << pvm_mytid() 
			 << " my Next is " << next 
			 << " and my Prev is " << prev  
			 << endl;


	//Recive Array From Prev item on Ring
		pvm_recv(prev, ARRAY_TAG);
		pvm_upkint(&recived_array_size, 1, 1);
		recived_array = new int[recived_array_size];
		pvm_upkint(recived_array, recived_array_size, 1);

		cout << "I am" << pvm_mytid() 
			 << " I Recived the following array " 
			 <<  array_to_string(recived_array,recived_array_size) 
			 << endl;

	//Processing recived array
		int new_array_size ;
		int *new_array = adjust_array(
							recived_array, 
							recived_array_size,
							&new_array_size,
							min_length,
							max_length
						);

		cout << "I am "  << pvm_mytid() 
			 << " I Generate the following array " 
			 <<  array_to_string(new_array,new_array_size) 
			 << endl;

	//Send Old and new Arrays To Parent
		pvm_initsend(PvmDataDefault);
		pvm_pkint(&tid,1,1);
		pvm_pkint(&next,1,1);
		pvm_pkint(&prev,1,1);

		pvm_pkint(&recived_array_size,1,1);
		pvm_pkint(recived_array,recived_array_size,1);

		pvm_pkint(&new_array_size,1,1);
		pvm_pkint(new_array, new_array_size, 1);


		pvm_send(pvm_parent(),ARRAY_TAG);


	//Send to Next item on Ring
		pvm_initsend(PvmDataDefault);
		pvm_pkint(&new_array_size,1,1);
		pvm_pkint(new_array, new_array_size, 1);
		pvm_send(next,ARRAY_TAG);


		return 0;
}

int next_prev(int *childs_ids,int childs_count,int ring_mode,int min_length,int max_length){
	for(int i=0 ; i < childs_count ; i++){
		int next,prev;

		if (ring_mode == FORWARD_MODE){
			if (i < childs_count - 1)
				next = childs_ids[i + 1];
			else
				next = pvm_mytid();

			if (i == 0)
				prev = pvm_mytid();
			else
				prev = childs_ids[i - 1];
		}

		if (ring_mode == BACKWARD_MODE){
			if (i > 0)
				next = childs_ids[i - 1];
			else
				next = pvm_mytid();

			if (i < childs_count - 1)
				prev = childs_ids[i + 1];
			else
				prev = pvm_mytid();
		}

		pvm_initsend(PvmDataDefault);
		pvm_pkint(&next, 1, 1);
		pvm_pkint(&prev, 1, 1);
		pvm_pkint(&min_length,1,1);
		pvm_pkint(&max_length,1,1);
		pvm_send(childs_ids[i], NEXT_PREV_TAG);
	}
	return 1;
}

int* adjust_array(int *old_array, int old_size, int *new_size, int min_length,int max_length) {
	*new_size = randInt(min_length, max_length);

	int* new_array = new int[*new_size];
	if (*new_size > old_size) {
		for (int i = 0; i < old_size; i++)
			new_array[i] = old_array[i];
		fill_array(new_array, old_size, *new_size);
	} else {
		for (int i = 0; i < *new_size; i++)
			new_array[i] = old_array[i];
	}
	return new_array;
}

int* generate_array(int minLengh, int maxLengh, int *size) {
	*size = randInt(minLengh, maxLengh);
	int *array = new int[*size];
	fill_array(array, 0, *size);
	return array;
}

string array_to_string(int *array, int size){
	stringstream str;
	str << "[";
	for (int i = 0; i < size; i++) {
		str << array[i];
		if ( i < size -1){
			str << ",";
		}
	}
	str << "]";
	return str.str();
}

void fill_array(int *array, int offset, int size) {
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> dis(MIN_VALUE, MAX_VALUE);

	for (int i = offset; i < size; i++)
		array[i] = dis(gen);
}

int randInt(int min, int max) {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(min, max);
	return dis(gen);
}
