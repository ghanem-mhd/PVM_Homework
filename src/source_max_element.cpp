/*

#include <iostream>
#include <time.h>
#include <sstream>
#include <stdlib.h>
#include <limits.h>

using namespace std;

#define MAX_VALUE 100
#define MIN_VALUE 0



int read_parameters(int *child_count, int *max_length, int *min_length);

int parent_work();

int* generate_array(int size);

int* adjust_array(int *old_array, int size);

int randInt(int min, int max);

void fill_array(int *array, int offset, int size);

string array_to_string(int *array, int size);

int* find_max_element(int *array, int *size,int *max);


int main(int argc, char* argv[]) {
	clock_t tStart = clock();
	parent_work();
	printf("\nTime taken: %.2fs\n", (double)(clock() - tStart));
	int t;
	cin >> t;
	return 0;
}

int read_parameters(int *child_count, int *max_length, int *min_length) {

	cout << "Enter number of childs\n";
	cin >> *child_count;
	if (*child_count <= 0){
		cout << "positive Number for childs count\n";
		return -1;
	}


	cout << "Enter Min Length\n";
	cin >> *min_length;
	if (*min_length <= 0) {
		cout << "positive Number for Min Length\n";
		return -1;
	}

	cout << "Enter Max Length\n";
	cin >> *max_length;
	if (*max_length <= 0) {
		cout << "positive Number for Max Length\n";
		return -1;
	}

	if (*max_length < *min_length){
		cout << "Max Length less than Min Length\n";
		return -1;
	}

	return 1;
}

int parent_work() {
	int childs_count = 3, ring_mode = 1, max_length = 10, min_length = 1 ;

	read_parameters(&childs_count, &max_length, &min_length);

	//Generate array with random length with random element
	int array_size=randInt(min_length,max_length);
	int *parent_array= generate_array(array_size);

	cout << "Parent\t: size "<<array_size <<", elements: "<< array_to_string(parent_array, array_size) << endl;

	for (int i = 0; i < childs_count; i++){
		int new_size = randInt(min_length, max_length);
		int *my_new_array = generate_array( new_size);
		if (array_size>new_size){
			for (int i = 0; i < new_size; i++){
				int max = INT_MIN;
				parent_array= find_max_element(parent_array, &array_size,&max);
				my_new_array[i] = max;
			}
		}
		else{
			for (int i = 0; i < array_size; i++){
				my_new_array[i] = parent_array[i];
			}
		}
		parent_array = my_new_array;
		array_size = new_size;
		cout << "\nchild["<<i<<"]: size "<<array_size<<", elements: ";
		cout<<array_to_string(parent_array, array_size);
	}


	return 0;
}



int* adjust_array(int *old_array, int size) {
	int * new_array = new int[size];

	int max_element_index;
	int max_element = INT_MIN;
	for (int i = 0; i < size; i++){
		new_array[i] = randInt(MIN_VALUE, MAX_VALUE);
		if (old_array[i] > max_element){
			max_element = old_array[i];
			max_element_index = i;
		}
	}
	new_array[max_element_index] = max_element;
	return new_array;
}

int* generate_array( int size) {

	int *array = new int[size];
	fill_array(array, 0, size);
	return array;
}

string array_to_string(int *array, int size){
	stringstream str;
	str << "[";
	for (int i = 0; i < size; i++) {
		str << array[i];
		if (i < size - 1){
			str << ",";
		}
	}
	str << "]";
	return str.str();
}

void fill_array(int *array, int offset, int size) {
	for (int i = offset; i < size; i++)
		array[i] = randInt(MIN_VALUE,MAX_VALUE);
}

int randInt(int min, int max) {
	return min + (rand() % (int)(max - min + 1));
}
int* find_max_element(int *array, int *size, int *max){
	int new_size = *size-1;
	//int *edited_array = new int[new_size];
	int index = 0;
	for (int i = 0; i < *size; i++){
		if (array[i]>*max){
			index = i;
			*max = array[i];
		}
	}

	int *temp = new int[new_size];
	int j = 0,  k = 0;
	while(j!=*size && k!=new_size){
		if (j!= index){
			temp[k] = array[j];
			k++;
		}
		j++;
	}

	*size = new_size;
	array = new int[new_size];
	k = 0;
	for (k = 0; k < new_size; k++){
		array[k]=temp[k];
	}
	return array;
}

*/
